package main

import (
	"git@bitbucket.org/romson/raspisanieonline/classes"
	"github.com/gin-gonic/gin"
	"git@bitbucket.org/romson/raspisanieonline/rooms"
)

func main() {
	router := gin.Default()

	classesRoutes := router.Group("/classes")
	{
		classesRoutes.GET("/check", classes.CheckHandler)
		classesRoutes.POST("/save", classes.SaveHandler)
		classesRoutes.POST("/delete", classes.DeleteHandler)
	}

	roomsRoutes := router.Group("/room")
	{
		roomsRoutes.POST("/save", rooms.SaveHandler)
		roomsRoutes.GET("/count", rooms.CountHandler)
	}

	router.Run(":4000")
}
