package rooms

import (
	"git@bitbucket.org/romson/raspisanieonline/classes"
	"git@bitbucket.org/romson/raspisanieonline/conf"
	"git@bitbucket.org/romson/raspisanieonline/request"
	"git@bitbucket.org/romson/raspisanieonline/rooms/service"
	"github.com/gin-gonic/gin"
	"net/http"
)

func SaveHandler(c *gin.Context) {
	c.Writer.Header().Set("Access-Control-Allow-Origin", conf.ACCESS_CONTROL_ALLOW_ORIGIN)

	class, _ := classes.DefineClass(request.REQUEST_TYPE_POST, c)

	if err := class.UpdateRoom(); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"status": false, "error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": true})
}

func CountHandler(c *gin.Context) {
	c.Writer.Header().Set("Access-Control-Allow-Origin", conf.ACCESS_CONTROL_ALLOW_ORIGIN)

	scheduleId, err := request.GetRequestInt("schedule_id", request.REQUEST_TYPE_GET, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"status": false, "error": err.Error()})
		return
	}

	roomId, err := request.GetRequestInt("rooms_id", request.REQUEST_TYPE_GET, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"status": false, "error": err.Error()})
		return
	}

	classNumberId, err := request.GetRequestInt("classes_numbers_id", request.REQUEST_TYPE_GET, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"status": false, "error": err.Error()})
		return
	}

	day, err := request.GetRequestInt("day", request.REQUEST_TYPE_GET, c)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"status": false, "error": err.Error()})
		return
	}

	number, err := service.Count(scheduleId, roomId, classNumberId, day)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"status": false, "error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": true, "number": number})
}
