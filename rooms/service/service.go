package service

import (
	"git@bitbucket.org/romson/raspisanieonline/db"
)

func Count(scheduleId, roomsId, classNumberId, day int) (int, error) {
	number := 0
	conn, err := db.NewMysql()

	if err != nil {
		return number, err
	}

	defer conn.Close()

	query := "select count(*) as number from classes where schedule_id = ? and rooms_id = ? and classes_numbers_id = ? and day = ?"
	err = conn.QueryRow(query, scheduleId, roomsId, classNumberId, day).Scan(&number)

	if err != nil {
		return number, err
	}

	return number, nil
}