package request

import (
	"database/sql"
	"github.com/gin-gonic/gin"
	"strconv"
	"errors"
)

const (
	REQUEST_TYPE_GET  int = 1
	REQUEST_TYPE_POST int = 2
)

func GetRequestInt(key string, rtype int, c *gin.Context) (int, error) {
	var query string

	if rtype == REQUEST_TYPE_GET {
		query = c.Query(key)
	} else if rtype == REQUEST_TYPE_POST {
		query = c.PostForm(key)
	}

	if query == "" {
		query = "0"
	}

	value, err := strconv.Atoi(query)

	if err != nil {
		return 0, errors.New(key + ": " + err.Error())
	}

	return value, nil
}

func GetRequestSqlInt64(key string, rtype int, c *gin.Context) (sql.NullInt64, error) {
	var query string

	if rtype == REQUEST_TYPE_GET {
		query = c.Query(key)
	} else if rtype == REQUEST_TYPE_POST {
		query = c.PostForm(key)
	}

	if query == "" {
		query = "0"
	}
	valueInt, err := strconv.Atoi(query)

	if err != nil {
		return sql.NullInt64{Int64: 0, Valid: false}, errors.New(key + ": " + err.Error())
	}

	return sql.NullInt64{Int64: int64(valueInt), Valid: valueInt != 0}, nil
}
