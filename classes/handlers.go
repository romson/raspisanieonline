package classes

import (
	"git@bitbucket.org/romson/raspisanieonline/classes/model"
	"git@bitbucket.org/romson/raspisanieonline/classes/validator"
	"git@bitbucket.org/romson/raspisanieonline/conf"
	"github.com/gin-gonic/gin"
	"net/http"
	"git@bitbucket.org/romson/raspisanieonline/request"
)

func CheckHandler(c *gin.Context) {
	c.Writer.Header().Set("Access-Control-Allow-Origin", conf.ACCESS_CONTROL_ALLOW_ORIGIN)

	class, errorArray := DefineClass(request.REQUEST_TYPE_GET, c)

	if len(errorArray) != 0 {
		c.JSON(http.StatusInternalServerError, gin.H{"status": false, "error": errorArray})
		return
	}

	valid := validator.New(class)

	vResult, err := valid.Validate()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"status": false, "error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": vResult, "messages": valid.Errors})
}

func SaveHandler(c *gin.Context) {
	c.Writer.Header().Set("Access-Control-Allow-Origin", conf.ACCESS_CONTROL_ALLOW_ORIGIN)

	class, _ := DefineClass(request.REQUEST_TYPE_POST, c)

	if err := class.Save(); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"status": false, "error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": true})
}

func DeleteHandler(c *gin.Context){
	c.Writer.Header().Set("Access-Control-Allow-Origin", conf.ACCESS_CONTROL_ALLOW_ORIGIN)

	class, _ := DefineClass(request.REQUEST_TYPE_POST, c)

	if err := class.Delete(); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"status": false, "error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"status": true})
}

func DefineClass(rtype int, c *gin.Context) (*model.Class, []string) {
	errorArray := make([]string, 0)

	scheduleId, err := request.GetRequestInt("schedule_id", rtype, c)
	if err != nil {
		errorArray = append(errorArray, err.Error())
	}

	subjectId, err := request.GetRequestInt("subject_id", rtype, c)
	if err != nil {
		errorArray = append(errorArray, err.Error())
	}

	teacherId, err := request.GetRequestInt("teacher_id", rtype, c)
	if err != nil {
		errorArray = append(errorArray, err.Error())
	}

	groupId, err := request.GetRequestInt("group_id", rtype, c)
	if err != nil {
		errorArray = append(errorArray, err.Error())
	}

	subgroupId, err := request.GetRequestSqlInt64("subgroup_id", rtype, c)
	if err != nil {
		errorArray = append(errorArray, err.Error())
	}

	roomId, err := request.GetRequestSqlInt64("room_id", rtype, c)
	if err != nil {
		errorArray = append(errorArray, err.Error())
	}

	classNumberId, err := request.GetRequestInt("classes_numbers_id", rtype, c)
	if err != nil {
		errorArray = append(errorArray, err.Error())
	}

	classTypeId, err := request.GetRequestSqlInt64("classes_types_id", rtype, c)
	if err != nil {
		errorArray = append(errorArray, err.Error())
	}

	day, err := request.GetRequestInt("day", rtype, c)
	if err != nil {
		errorArray = append(errorArray, err.Error())
	}

	class := model.NewClass(scheduleId, subjectId, teacherId, groupId, subgroupId, roomId, classNumberId, classTypeId, day)

	return class, errorArray
}
