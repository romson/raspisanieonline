package model

import (
	"database/sql"
	"git@bitbucket.org/romson/raspisanieonline/db"
)

type Class struct {
	ScheduleId    int
	SubjectId     int
	TeacherId     int
	GroupId       int
	SubgroupId    sql.NullInt64
	RoomId        sql.NullInt64
	ClassNumberId int
	ClassTypeId   sql.NullInt64
	Day           int
	periodId      int
	teacherName   string
	subjectTitle  string
	groupTitle string
}

func NewClass(scheduleId, subjectId, teacherId, groupId int, subgroupId, roomId sql.NullInt64, classNumberId int, classTypeId sql.NullInt64, day int) *Class {
	return &Class{
		scheduleId,
		subjectId,
		teacherId,
		groupId,
		subgroupId,
		roomId,
		classNumberId,
		classTypeId,
		day,
		0,
		"",
		"",
		""}
}

func (c *Class) Save() error {
	conn, err := db.NewMysql()

	if err != nil {
		return err
	}

	defer conn.Close()

	query := `insert into classes (schedule_id, subjects_id, teachers_id, groups_id, subgroups_id, rooms_id, classes_numbers_id, classes_types_id, day) values
	(?,?,?,?,?,?,?,?,?)`

	if _, err := conn.Exec(query, c.ScheduleId, c.SubjectId, c.TeacherId, c.GroupId, c.SubgroupId, c.RoomId, c.ClassNumberId, c.ClassTypeId, c.Day); err != nil {
		return err
	}

	return nil
}

func (c *Class) UpdateRoom() error {
	conn, err := db.NewMysql()

	if err != nil {
		return err
	}

	defer conn.Close()

	query := `update classes set rooms_id = ?
	where
		schedule_id = ?
		and subjects_id = ?
		and teachers_id = ?
		and groups_id = ?
		and classes_numbers_id = ?
		and day = ?`

	if _, err := conn.Exec(query, c.RoomId, c.ScheduleId, c.SubjectId, c.TeacherId, c.GroupId, c.ClassNumberId, c.Day); err != nil {
		return err
	}

	return nil
}

func (c *Class) Delete() error {
	conn, err := db.NewMysql()

	if err != nil {
		return err
	}

	defer conn.Close()

	query := `delete from classes
	where
		schedule_id = ?
		and subjects_id = ?
		and teachers_id = ?
		and groups_id = ?
		and classes_numbers_id = ?
		and day = ?`

	if _, err := conn.Exec(query, c.ScheduleId, c.SubjectId, c.TeacherId, c.GroupId, c.ClassNumberId, c.Day); err != nil {
		return err
	}

	return nil
}

func (c *Class) GetPeriodId() (int, error) {
	if c.periodId > 0 {
		return c.periodId, nil
	}

	conn, err := db.NewMysql()

	if err != nil {
		return 0, err
	}

	defer conn.Close()

	conn.QueryRow("select periods_id from schedule where id = ?", c.ScheduleId).Scan(&c.periodId)

	return c.periodId, nil
}

func (c *Class) GetTeacherName() (string, error) {
	if c.teacherName != "" {
		return c.teacherName, nil
	}

	conn, err := db.NewMysql()

	if err != nil {
		return "", err
	}

	defer conn.Close()

	conn.QueryRow("select concat(lastname, ' ', substr(firstname, 1, 1), '. ', substr(middlename, 1, 1), '.') from teachers where id = ?", c.TeacherId).Scan(&c.teacherName)

	return c.teacherName, nil
}

func (c *Class) GetSubjectTitle() (string, error) {
	if c.subjectTitle != "" {
		return c.subjectTitle, nil
	}

	conn, err := db.NewMysql()

	if err != nil {
		return "", err
	}

	defer conn.Close()

	conn.QueryRow("select title_short from subjects where id = ?", c.SubjectId).Scan(&c.subjectTitle)

	return c.subjectTitle, nil
}

func (c *Class) GetGroupTitle() (string, error) {
	if c.groupTitle != "" {
		return c.groupTitle, nil
	}

	conn, err := db.NewMysql()

	if err != nil {
		return "", err
	}

	defer conn.Close()

	query := `select concat(gr.level, g.letter) as title
	from groups as g
	inner join grades as gr on g.id = gr.groups_id
	inner join periods as p on p.id = ? and gr.begin_year = p.begin_year
	where g.id = ?`

	periodId, err := c.GetPeriodId()

	if err != nil {
		return "", err
	}

	conn.QueryRow(query, periodId, c.GroupId).Scan(&c.groupTitle)

	return c.groupTitle, nil
}
