package validator

import (
	"fmt"
	"git@bitbucket.org/romson/raspisanieonline/classes/model"
	"git@bitbucket.org/romson/raspisanieonline/db"
	"database/sql"
)

var validations = []func(*Validator, chan int, chan int, chan error){
	(*Validator).validateTeacherClassNumber,
	(*Validator).validateTeacherSubjectWorkload,
	(*Validator).validateTeacherTotalWorkload,
	(*Validator).validateTeacherGroupSubject,
	(*Validator).validateGroupSubjectWorkload,
	(*Validator).validateGroupTotalWorkload,
	(*Validator).validateTeacherSubgroup,
}

type Validator struct {
	Class  *model.Class
	Errors []string
}

func New(c *model.Class) *Validator {
	return &Validator{c, make([]string, 0)}
}

func (v *Validator) Validate() (bool, error) {
	ce := make(chan error)
	c := make(chan int)
	cl := c
	cr := c

	for _, fn := range validations {
		cr = make(chan int)
		go fn(v, cl, cr, ce)
		cl = cr
	}

	go func(c chan int) { c <- 0 }(cr)

	for {
		select {
		case number := <-c:
			if number == len(validations) {
				if len(v.Errors) == 0 {
					return true, nil
				} else {
					return false, nil
				}
			}
		case err := <-ce:
			return false, err
		}
	}
}

func (v *Validator) addError(message string) {
	v.Errors = append(v.Errors, message)
}

// Check whether a teacher has already been set on the same class number in a particular day
func (v *Validator) validateTeacherClassNumber(cl chan int, cr chan int, ce chan error) {
	number := 0
	conn, err := db.NewMysql()

	if err != nil {
		ce <- err
		return
	}

	defer conn.Close()

	sql := "select count(*) as number from classes where schedule_id = ? and teachers_id = ? and classes_numbers_id = ? and day = ?"

	st, err := conn.Prepare(sql)

	if err != nil {
		ce <- err
		return
	}

	defer st.Close()

	row := st.QueryRow(v.Class.ScheduleId, v.Class.TeacherId, v.Class.ClassNumberId, v.Class.Day)
	err = row.Scan(&number)

	if err != nil {
		ce <- err
		return
	}

	if number > 0 {
		teacherName, err := v.Class.GetTeacherName()

		if err != nil {
			ce <- err
			return
		}

		v.addError(fmt.Sprintf("Преподаватель %s уже назначен на урок в другом классе в этот день!", teacherName))
	}

	cl <- 1 + <-cr
}

// Check a teacher's workload on a particular subject
func (v *Validator) validateTeacherSubjectWorkload(cl chan int, cr chan int, ce chan error) {
	var plannedHours, actualHours int

	conn, err := db.NewMysql()

	if err != nil {
		ce <- err
		return
	}

	defer conn.Close()

	periodId, err := v.Class.GetPeriodId()

	if err != nil {
		ce <- err
		return
	}

	conn.QueryRow("select sum(hours) as hours from curriculum where periods_id = ? and teachers_id = ? and subjects_id = ?", periodId, v.Class.TeacherId, v.Class.SubjectId).Scan(&plannedHours)
	conn.QueryRow("select count(*) as hours from classes where schedule_id = ? and teachers_id = ? and subjects_id = ?", v.Class.ScheduleId, v.Class.TeacherId, v.Class.SubjectId).Scan(&actualHours)

	if actualHours+1 > plannedHours {
		teacherName, err := v.Class.GetTeacherName()

		if err != nil {
			ce <- err
			return
		}

		subjectTitle, err := v.Class.GetSubjectTitle()

		if err != nil {
			ce <- err
			return
		}

		v.addError(fmt.Sprintf("Для преподавателя %s превышена нагрузка по предмету %s!", teacherName, subjectTitle))
	}

	cl <- 1 + <-cr
}

// Check a teacher's total workload
func (v *Validator) validateTeacherTotalWorkload(cl chan int, cr chan int, ce chan error) {
	var plannedHours, actualHours int

	conn, err := db.NewMysql()

	if err != nil {
		ce <- err
		return
	}

	defer conn.Close()

	periodId, err := v.Class.GetPeriodId()

	if err != nil {
		ce <- err
		return
	}

	conn.QueryRow("select sum(hours) as hours from curriculum where periods_id = ? and teachers_id = ?", periodId, v.Class.TeacherId).Scan(&plannedHours)
	conn.QueryRow("select count(*) as hours from classes where schedule_id = ? and teachers_id = ?", v.Class.ScheduleId, v.Class.TeacherId).Scan(&actualHours)

	if actualHours+1 > plannedHours {
		teacherName, err := v.Class.GetTeacherName()

		if err != nil {
			ce <- err
			return
		}

		v.addError(fmt.Sprintf("Для преподавателя %s превышена общая нагрузка!", teacherName))
	}

	cl <- 1 + <-cr
}

// Check whether a teacher is linked with a group for the particular subject in the curriculum
func (v *Validator) validateTeacherGroupSubject(cl chan int, cr chan int, ce chan error){
	hours := 0

	conn, err := db.NewMysql()

	periodId, err := v.Class.GetPeriodId()

	if err != nil {
		ce <- err
		return
	}

	conn.QueryRow("select hours from curriculum where periods_id = ? and teachers_id = ? and subjects_id = ? and groups_id = ?", periodId, v.Class.TeacherId, v.Class.SubjectId, v.Class.GroupId).Scan(&hours)

	if hours > 0 {
		if err := v.validateGroupTeacherSubjectHours(conn, hours); err != nil {
			ce <- err
			return
		}
	} else {
		teacherName, err := v.Class.GetTeacherName()

		if err != nil {
			ce <- err
			return
		}

		subjectTitle, err := v.Class.GetSubjectTitle()

		if err != nil {
			ce <- err
			return
		}

		groupTitle, err := v.Class.GetGroupTitle()

		if err != nil {
			ce <- err
			return
		}

		v.addError(fmt.Sprintf("Преподаватель %s не назначен проводить %s в %s классе!", teacherName, subjectTitle, groupTitle))
	}

	cl <- 1 + <-cr
}

// Check a group's workload on a particular subject by a particular teacher
func (v *Validator) validateGroupTeacherSubjectHours(conn *sql.DB, teacherHours int) error {
	groupHours := 0

	conn.QueryRow("select count(*) as hours from classes where schedule_id = ? and groups_id = ? and teachers_id = ? and subjects_id = ?", v.Class.ScheduleId, v.Class.GroupId, v.Class.TeacherId, v.Class.SubjectId).Scan(&groupHours)

	if groupHours + 1 > teacherHours {
		teacherName, err := v.Class.GetTeacherName()

		if err != nil {
			return err
		}

		subjectTitle, err := v.Class.GetSubjectTitle()

		if err != nil {
			return err
		}

		v.addError(fmt.Sprintf("Загруженность класса по предмету %s для преподавателя %s превышена!", subjectTitle, teacherName))
	}

	return nil
}

// Check a group's total workload on a particular subject
func (v *Validator) validateGroupSubjectWorkload(cl chan int, cr chan int, ce chan error){
	var plannedHours, actualHours int

	conn, err := db.NewMysql()

	if err != nil {
		ce <- err
		return
	}

	defer conn.Close()

	periodId, err := v.Class.GetPeriodId()

	if err != nil {
		ce <- err
		return
	}

	conn.QueryRow("select sum(hours) as hours from curriculum where periods_id = ? and subjects_id = ? and groups_id = ?", periodId, v.Class.SubjectId, v.Class.GroupId).Scan(&plannedHours)
	conn.QueryRow("select count(*) as hours from classes where schedule_id = ? and subjects_id = ? and groups_id = ?", v.Class.ScheduleId, v.Class.SubjectId, v.Class.GroupId).Scan(&actualHours)

	if actualHours+1 > plannedHours {
		groupTitle, err := v.Class.GetGroupTitle()

		if err != nil {
			ce <- err
			return
		}

		subjectTitle, err := v.Class.GetSubjectTitle()

		if err != nil {
			ce <- err
			return
		}

		v.addError(fmt.Sprintf("Для класса %s превышена загрузка по предмету %s!", groupTitle, subjectTitle))
	}

	cl <- 1 + <-cr
}

// Check a group's total workload
func (v *Validator) validateGroupTotalWorkload(cl chan int, cr chan int, ce chan error){
	var plannedHours, actualHours int

	conn, err := db.NewMysql()

	if err != nil {
		ce <- err
		return
	}

	defer conn.Close()

	periodId, err := v.Class.GetPeriodId()

	if err != nil {
		ce <- err
		return
	}

	conn.QueryRow("select sum(hours) as hours from curriculum where periods_id = ? and groups_id = ?", periodId, v.Class.GroupId).Scan(&plannedHours)
	conn.QueryRow("select count(*) as hours from classes where schedule_id = ? and groups_id = ?", v.Class.ScheduleId, v.Class.GroupId).Scan(&actualHours)

	if actualHours+1 > plannedHours {
		groupTitle, err := v.Class.GetGroupTitle()

		if err != nil {
			ce <- err
			return
		}

		v.addError(fmt.Sprintf("Общая нагрузка для класса %s превышена!", groupTitle))
	}

	cl <- 1 + <-cr
}

// Check whether a teacher is linked with one of a group's subgroups, if a class is already set
func (v *Validator) validateTeacherSubgroup(cl chan int, cr chan int, ce chan error){
	var isSet, isPlanned int

	conn, err := db.NewMysql()

	if err != nil {
		ce <- err
		return
	}

	defer conn.Close()

	queryIsSet := "select if(count(*) > 0, 1, 0) as is_set from classes where schedule_id = ? and groups_id = ? and classes_numbers_id = ? and day = ?"
	conn.QueryRow(queryIsSet, v.Class.ScheduleId, v.Class.GroupId, v.Class.ClassNumberId, v.Class.Day).Scan(&isSet)

	if isSet == 1 {
		periodId, err := v.Class.GetPeriodId()

		if err != nil {
			ce <- err
			return
		}

		queryIsPlanned := "select if( count(*) > 0, 1, 0) as is_set from curriculum where periods_id = ? and teachers_id = ? and groups_id = ? and subgroups_id > 0"
		conn.QueryRow(queryIsPlanned, periodId, v.Class.TeacherId, v.Class.GroupId).Scan(&isPlanned)

		if isPlanned == 0 {
			teacherName, err := v.Class.GetTeacherName()

			if err != nil {
				ce <- err
				return
			}

			groupTitle, err := v.Class.GetGroupTitle()

			if err != nil {
				ce <- err
				return
			}

			v.addError(fmt.Sprintf("Данный урок занят, преподаватель %s не установлен для подгруппы в %s классе!", teacherName, groupTitle))
		}
	}

	cl <- 1 + <-cr
}