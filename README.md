# Raspisanieonline

This is an uncompleted microservice of a system for scheduling classes
in educational establishments.
This service is aimed to check if a teacher is available for a class
which a user wants to set it for. It checks teacher's workload
and availability, group's and room's availability.

Since the project is frozen this service is used as an example of GO code.

