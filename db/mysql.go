package db

import (
	_ "github.com/go-sql-driver/mysql"
	"database/sql"
	"git@bitbucket.org/romson/raspisanieonline/conf"
)

func NewMysql() (*sql.DB, error) {
	sourceString := conf.DB_USER + ":" + conf.DB_PASSWORD + "@tcp(" + conf.DB_HOST + ":3306)/" + conf.DB_NAME
	sourceString += "?collation=utf8_general_ci"
	return sql.Open("mysql", sourceString)
}